package poxizcraft.com.br.pcperfil;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	/*
	Ol�,
	
	Eu Liberei A Src Do Plugin, Mas Isso N�o Significa Que Voc� Pode Dizer Que Voc� O Fez, Isso Se Chama Plagio, Deixe Os Creditos =)
	Porem, Voc� Pode "Alterar" O Codigo, Da Forma Que Mais Lhe Agrada Para Seu Uso.
	
	Bom, Esse Plugin N�o Est� Completo... Aconselho Voc� A Usar Uns Regex, Para Que Os Jogadores N�o Coloquem Besteiras
   	No Perfil (:
   	
   	OBS: Eu Criei Esse Plugin Quando Estava Aprendendo Java... Eu Acho O Codigo Mal-Organizado, E Tem "Formas" Melhores De
   	Fazer Algumas Instru��es...
	
	Atenciosamente, SrForYou.
	
	Autor Do Plugin: SrForYou.
	Meu Skype: srforyou.new
	
	 */
	
		public void onEnable() {
					Bukkit.getConsoleSender().sendMessage("[PC-PERFIL] Status Plugin: Ativado");
					Bukkit.getServer().getPluginManager().registerEvents(this, this);
					saveDefaultConfig();
	}	 
					
		public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		    Player p = (Player)sender;
		    if(commandLabel.equalsIgnoreCase("Perfil")){
		    	if(!p.hasPermission("pcperfil.usar")){
		    		p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Tem Permissao.");
		    		return true;
		    	}			    	
		    	if(args.length == 0){
		    		p.sendMessage("�8[�bPerfil�8] �fDigite: �8/�bPerfil �f?");
		    	}else{	    		
		    		if(args[0].equalsIgnoreCase("?")){	 
		    		p.sendMessage("�8==][=========================================][==");
		    		p.sendMessage("");
		    		p.sendMessage("    �f�lComandos:");
		    		p.sendMessage("        �8�l/�bPerfil �8[�bNick�8]");
		    		p.sendMessage("        �8�l/�bPerfil �8[�bCriar�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil");
		    		p.sendMessage("");
		    		p.sendMessage("�8==][=========================================][==");
		    		return true;
		    		}
		    	if(args.length == 1){	
		    		if(args[0].equalsIgnoreCase("Criar")){	    		
					    if(getConfig().contains("Perfil-De-"+p.getName())){
					    	p.sendMessage("�8[�bPerfil�8] �fVoc� Ja Tem Um Perfil.");
					    	return true;
					    }
						getConfig().set("Perfil-De-"+p.getName()+".Nome", "???");
						getConfig().set("Perfil-De-"+p.getName()+".Idade", "???");
						getConfig().set("Perfil-De-"+p.getName()+".Skype", "???");
						getConfig().set("Perfil-De-"+p.getName()+".Sexo", "???");
						getConfig().set("Perfil-De-"+p.getName()+".WhatsApp", "???");
						getConfig().set("Perfil-De-"+p.getName()+".Instagram", "???");
					    p.sendMessage("�8[�bPerfil�8] �fPerfil Criado Com Sucesso!");
						saveConfig();					    	
						return true;
				    }
		    	if(args[0].equalsIgnoreCase(args[0])){
		    		if(!getConfig().contains("Perfil-De-"+args[0])){
		    				p.sendMessage("�8[�bPerfil�8] �fPerfil Inexistente.");		  
		    			}else{
		    				AbrirPerfil(args[0], p);
		    					}
		    				}
						return true;
		    		}
		    	}
		    }
		    if(commandLabel.equalsIgnoreCase("EditarPerfil")) {
		    	if(!p.hasPermission("editarperfil.use")){
		    		p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Tem Permissao.");
		    		return true;
		    	}
		    	if(!getConfig().contains("Perfil-De-"+p.getName())){
		    		p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Tem Um Perfil.");
		    		return true;
		    	}
		    	if(args.length == 0){
		    		p.sendMessage("�8==][=========================================][==");
		    		p.sendMessage("");
		    		p.sendMessage("    �f�lComo Editar Seu Perfil:");
		    		p.sendMessage("        �8�l/�bEditarPerfil Nome �8[�bNome�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil Idade �8[�bIdade�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil Skype �8[�bSkype�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil WhatsApp �8[�bWhatsApp�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil Instagram �8[�bInstagram�8]");
		    		p.sendMessage("        �8�l/�bEditarPerfil Sexo �8[�bMasculino�f/�bFeminino�8]");
		    		p.sendMessage("        �8�l/�cEditarPerfil Excluir");
		    		p.sendMessage("");
		    		p.sendMessage("�8==][=========================================][==");
		    		return true;
		    	}else{
		    		if(args[0].equalsIgnoreCase("Excluir")){
		    			p.sendMessage("�8[�bPerfil�8] �cVoc� Excluiu Seu Perfil!!!");
		    			getConfig().set("Perfil-De-"+p.getName(), null);
		    			saveConfig();
		    			return true;
	    				}
		    		if(args[0].equalsIgnoreCase("Nome")){
			    		String NomeC;
		    				if(args.length == 1){
		    				p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Seu Nome.");
		    				return true;
		    			}else if(args.length == 2){
		    				NomeC = args[1];
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome �: "+NomeC);
		    				getConfig().set("Perfil-De-"+p.getName()+".Nome", NomeC);
		    				saveConfig();
		    				return true;
		    			}
		    			if(args.length == 3){
		    				NomeC = args[1]+" "+args[2];
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome �: "+NomeC);
		    				getConfig().set("Perfil-De-"+p.getName()+".Nome", NomeC);
		    				saveConfig();
		    				return true;	
		    			}
		    			if(args.length == 4){
		    				NomeC = args[1]+" "+args[2]+" "+args[3];
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome �: "+NomeC);
		    				getConfig().set("Perfil-De-"+p.getName()+".Nome", NomeC);
		    				saveConfig();
		    				return true;	
		    			}
		    			if(args.length == 5){
		    				NomeC = args[1]+" "+args[2]+" "+args[3]+" "+args[4];
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome �: "+NomeC);
		    				getConfig().set("Perfil-De-"+p.getName()+".Nome", NomeC);
		    				saveConfig();
		    				return true;	
		    			}
		    			if(args.length == 6){
		    				NomeC = args[1]+" "+args[2]+" "+args[3]+" "+args[4]+" "+args[5];
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome �: "+NomeC);
		    				getConfig().set("Perfil-De-"+p.getName()+".Nome", NomeC);
		    				saveConfig();
		    				return true;	
		    			}
		    			if(args.length > 6){
		    				p.sendMessage("�8[�bPerfil�8] �fSeu Nome � Muito Grande");
		    				return true;
		    			}
		    		}
		    		if(args[0].equalsIgnoreCase("Idade")){
		    				if(args.length == 1){
		    				p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Sua Idade.");
		    				return true;
		    			}else if(args.length == 2){
		    					String IdadeC = args[1];
				    			p.sendMessage("�8[�bPerfil�8] �fSua Idade �: "+IdadeC);
				    			getConfig().set("Perfil-De-"+p.getName()+".Idade", IdadeC);
				    			saveConfig();
				    			return true;
		    				}
			    			if(args.length > 2){
			    				p.sendMessage("�8[�bPerfil�8] �fVoc� Tem Quantas Idades?");
			    				return true;
			    			}
			    		}
		    		if(args[0].equalsIgnoreCase("Sexo")){
	    				if(args.length == 1){
	    				p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Seu Sexo.");
	    				return true;
	    			}else if(args.length == 2){
	    					String SexoC = args[1];
			    			p.sendMessage("�8[�bPerfil�8] �fSeu Sexo �: "+SexoC);
			    			getConfig().set("Perfil-De-"+p.getName()+".Sexo", SexoC);
			    			saveConfig();
			    			return true;
	    				}
		    			if(args.length > 2){
		    				p.sendMessage("�8[�bPerfil�8] �fVoc� Tem Quantos Sexos?");
		    				return true;
		    			}
		    		}
		    		if(args[0].equalsIgnoreCase("Skype")){
	    					if(args.length == 1){
	    					p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Seu Skype.");
	    					return true;
	    			}else if(args.length == 2){
	    					String SkypeC = args[1];
	    					p.sendMessage("�8[�bPerfil�8] �fSeu Skype �: "+SkypeC);
	    					getConfig().set("Perfil-De-"+p.getName()+".Skype", SkypeC);
	    					saveConfig();
	    			}
		    		if(args.length > 2){
		    				p.sendMessage("�8[�bPerfil�8] �fApenas Um Skype.");
		    				return true;
		    			}
		    		}
		    		if(args[0].equalsIgnoreCase("WhatsApp")){
						if(args.length == 1){
						p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Seu WhatsApp.");
						return true;
				}else if(args.length == 2){
						String WhatsAppC = args[1];
						p.sendMessage("�8[�bPerfil�8] �fSeu WhatsApp �: "+WhatsAppC);
						getConfig().set("Perfil-De-"+p.getName()+".WhatsApp", WhatsAppC);
						saveConfig();
					}
	    		if(args.length > 2){
	    				p.sendMessage("�8[�bPerfil�8] �fVoc� So Pode Colocar Um WhatsApp");
	    				return true;
	    				}
		    		}
		    	if(args[0].equalsIgnoreCase("Instagram")){
	    			if(args.length == 1){
	    			p.sendMessage("�8[�bPerfil�8] �fVoc� N�o Colocou Seu Instagram.");
	    			return true;
	    		}else if(args.length == 2){
	    			String InstagramC = args[1];
	    			p.sendMessage("�8[�bPerfil�8] �fSeu Instagram �: "+"@"+InstagramC);
	    			getConfig().set("Perfil-De-"+p.getName()+".Instagram", "@"+InstagramC);
	    			saveConfig();
	    		}
	    		if(args.length > 2){
	    			p.sendMessage("�8[�bPerfil�8] �fVoc� Tem Quantos Instagram?");
	    			return true;
	    		}
	    				}
		    		}
		    	}
		    return false;
		}	
		
		public void AbrirPerfil(String PerfilDeQuem, Player AbrirParaQuem) {
			
			String Nome = getConfig().getString("Perfil-De-"+PerfilDeQuem+".Nome");
	    	String Idade = getConfig().getString("Perfil-De-"+PerfilDeQuem+".Idade");
	    	String Skype = getConfig().getString("Perfil-De-"+PerfilDeQuem+".Skype");
	    	String Sexo = getConfig().getString("Perfil-De-"+PerfilDeQuem+".Sexo");
	    	String WhatsApp = getConfig().getString("Perfil-De-"+PerfilDeQuem+".WhatsApp");
	    	String Instagram = getConfig().getString("Perfil-De-"+PerfilDeQuem+".Instagram");
	    	
			Inventory inv = Bukkit.getServer().createInventory(AbrirParaQuem, 54, "�bPerfil");

				ItemStack GlassPlane = new ItemStack(Material.THIN_GLASS);
					ItemMeta GlassPlane2 = GlassPlane.getItemMeta();
						GlassPlane2.setDisplayName("�6");
						GlassPlane.setItemMeta(GlassPlane2);
			
				ItemStack Vinhas = new ItemStack(Material.VINE);
		   			ItemMeta Vinhas2 = Vinhas.getItemMeta();
		   				Vinhas2.setDisplayName("�6");
		   				Vinhas.setItemMeta(Vinhas2);
			
		    	@SuppressWarnings("deprecation")
				ItemStack HeadNome = new ItemStack(397, 1, (short) 3);
		    		ItemMeta HeadNome2 = HeadNome.getItemMeta();
		    			ArrayList<String> Lore = new ArrayList<String>();
		    			Lore.add("�8"+Nome+"");
		    			HeadNome2.setLore(Lore);
		    			HeadNome2.setDisplayName("�bNome:");
		    			HeadNome.setItemMeta(HeadNome2);	    			
		    			
		    	ItemStack AnvilIdade = new ItemStack(Material.ANVIL);
			    	ItemMeta AnvilIdade2 = AnvilIdade.getItemMeta();
		    			ArrayList<String> Lore1 = new ArrayList<String>();
	    				Lore1.add("�8"+Idade+"");
	    				AnvilIdade2.setLore(Lore1);
			    		AnvilIdade2.setDisplayName("�bIdade:");
			    		AnvilIdade.setItemMeta(AnvilIdade2);
		    			
				ItemStack BookSkype = new ItemStack(Material.ENCHANTED_BOOK);
				   	ItemMeta Skype2 = BookSkype.getItemMeta();
				   		ArrayList<String> Lore2 = new ArrayList<String>();
						Lore2.add("�8"+Skype+"");
						Skype2.setLore(Lore2);
				   		Skype2.setDisplayName("�bSkype:");
				   		BookSkype.setItemMeta(Skype2);
					   		
				ItemStack BookWhatsApp = new ItemStack(Material.ENCHANTED_BOOK);
				   	ItemMeta WhatsApp2 = BookWhatsApp.getItemMeta();
				   		ArrayList<String> Lore4 = new ArrayList<String>();
						Lore4.add("�8"+WhatsApp+"");
						WhatsApp2.setLore(Lore4);
				   		WhatsApp2.setDisplayName("�bWhatsApp");
				   		BookWhatsApp.setItemMeta(WhatsApp2);
							   		
				ItemStack BookInstagram = new ItemStack(Material.ENCHANTED_BOOK);
					ItemMeta Instagram2 = BookInstagram.getItemMeta();
		    			ArrayList<String> Lore6 = new ArrayList<String>();
	    				Lore6.add("�8"+Instagram+"");
	    				Instagram2.setLore(Lore6);
					   	Instagram2.setDisplayName("�bInstagram:");
					   	BookInstagram.setItemMeta(Instagram2);
						   	
				ItemStack BookSexo = new ItemStack(Material.ENCHANTED_BOOK);
					ItemMeta BookSexo2 = BookSexo.getItemMeta();
						ArrayList<String> Lore7 = new ArrayList<String>();
						Lore7.add("�8"+Sexo+"");
						BookSexo2.setLore(Lore7);
						BookSexo2.setDisplayName("�bSexo:");
						BookSexo.setItemMeta(BookSexo2);
						
						   	inv.setItem(0, Vinhas);
						   	inv.setItem(1, Vinhas);
						   	inv.setItem(2, Vinhas);
						   	inv.setItem(3, GlassPlane);
						   	inv.setItem(4, GlassPlane);
						   	inv.setItem(5, GlassPlane);
						   	inv.setItem(6, Vinhas);
						   	inv.setItem(7, Vinhas);
						   	inv.setItem(8, Vinhas);
						   	inv.setItem(9, Vinhas);
						   	inv.setItem(10, GlassPlane);
						   	inv.setItem(11, GlassPlane);
						   	inv.setItem(12, GlassPlane);
						   	
						   	inv.setItem(13, AnvilIdade);
						   	
						   	inv.setItem(14, GlassPlane);
						   	inv.setItem(15, GlassPlane);
						   	inv.setItem(16, GlassPlane);
						   	inv.setItem(17, Vinhas);
						   	inv.setItem(18, Vinhas);
						   	inv.setItem(19, GlassPlane);
						   	
						   	inv.setItem(20, BookSkype);
						   	
						   	inv.setItem(21, AnvilIdade);
						   	
						   	inv.setItem(22, HeadNome);
						   	
						   	inv.setItem(23, AnvilIdade);					   
						   	
						   	inv.setItem(24, BookWhatsApp);
						   	
						   	inv.setItem(25, GlassPlane);
						   	
						   	inv.setItem(26, Vinhas);
						   	inv.setItem(27, Vinhas);
						   	inv.setItem(28, GlassPlane);
						   	inv.setItem(29, GlassPlane);
						   	
						   	inv.setItem(30, BookSexo);
						   	
						   	inv.setItem(31, AnvilIdade);
						   	
						   	inv.setItem(32, BookInstagram);
						   	
						   	inv.setItem(33, GlassPlane);
						   	inv.setItem(34, GlassPlane);
						   	inv.setItem(35, Vinhas);
						   	inv.setItem(36, Vinhas);
						   	inv.setItem(37, Vinhas);
						   	inv.setItem(38, GlassPlane);
						   	inv.setItem(39, GlassPlane);
						   	inv.setItem(40, GlassPlane);
						   	inv.setItem(41, GlassPlane);
						   	inv.setItem(42, GlassPlane);	
						   	inv.setItem(43, Vinhas);
						   	inv.setItem(44, Vinhas);
						   	inv.setItem(45, Vinhas);
						   	inv.setItem(46, Vinhas);
						   	inv.setItem(47, Vinhas);
						   	inv.setItem(48, GlassPlane);
						   	inv.setItem(49, GlassPlane);
						   	inv.setItem(50, GlassPlane);
						   	inv.setItem(51, Vinhas);
						   	inv.setItem(52, Vinhas);
						   	inv.setItem(53, Vinhas);
						   	
						   	AbrirParaQuem.openInventory(inv);
		}
		
		@SuppressWarnings("deprecation")
		@EventHandler
		public void onInventory(InventoryClickEvent e){
		    if ((e.getInventory().getTitle().equalsIgnoreCase("�bPerfil")) && 
		      (e.getCurrentItem() != null) && (e.getCurrentItem().getTypeId() != 0))
		    {
		      if (e.getCurrentItem().getType() == Material.VINE)
		      {
		        e.setCancelled(true);
		        
		        return;
		      }
		      if (e.getCurrentItem().getType() == Material.THIN_GLASS)
		      {
		        e.setCancelled(true);
		        return;
		      }
		      if (e.getCurrentItem().getType() == Material.SKULL_ITEM)
		      {
		        e.setCancelled(true);
		        return;
		      }
		      if (e.getCurrentItem().getType() == Material.ENCHANTED_BOOK)
		      {
		        e.setCancelled(true);
		        return;
		      }
		      if (e.getCurrentItem().getType() == Material.ANVIL)
		      {
		        e.setCancelled(true);
		        return;
		      }
		}
	}
		
	@EventHandler
	public void onBrincalhaoLogin(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		if(p.getName().equalsIgnoreCase("Criar")) {
			e.setKickMessage("�8Sae Dae Doido \n ... \n SeMoi PlayBoy \n ...\n Querendo Bugar O Plugin Seu Disgra�ado \n ... \n �8OBS�8: �dSrForYou�8 � Lindo.");
			e.setResult(Result.KICK_OTHER);
			return;
		}
	}
}